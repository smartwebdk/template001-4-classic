{includeScript file='partials/body.js.tpl'}

{*** Languages collection ***}
{collection controller=language assign=languages}

{*** Get all shop currencies ***}
{collection controller=currency assign=currencies}

{if $template.settings.SETTINGS_SHOW_SEARCH or $currencies and $currencies->getActualSize() gt 1 or $languages->getActualSize() gt 1 or !empty($text.TOP_USP_1) or !empty($text.TOP_USP_2) or !empty($text.TOP_USP_3)}
<div class="container container--corporate">
    <div class="site-corporate">
        <div class="row">
            <div class="col-s-4 col-m-12 col-l-12 col-xl-24">
                {if $languages->getActualSize() gt 1 || $currencies and $currencies->getActualSize() gt 1}
                    <div class="pull-left is-inline-block dropdown-pickers is-hidden-s">
                        {if $languages->getActualSize() gt 1}
                            <div class="w-language-picker is-inline-block">
                                {$showFlag = true}
                                {$showText = true}
                                {if $template.settings.SETTINGS_TYPE_LANGUAGE eq 'FLAG'}
                                    {$showText = false}
                                {elseif $template.settings.SETTINGS_TYPE_LANGUAGE eq 'TEXT'}
                                    {$showFlag = false}
                                {/if}

                                {include file="modules/widgets/language/language.tpl" collection=$languages type="dropdown" showFlag=$showFlag showText=$showText}
                            </div>
                        {/if}

                        {if $currencies and $currencies->getActualSize() gt 1}
                            <div class="w-currency-picker is-inline-block">
                                {include file="modules/widgets/currency/currency.tpl" collection=$currencies type="dropdown"}
                            </div>
                        {/if}
                    </div>
                {/if}
                
                {if !empty($text.TOP_USP_1) or !empty($text.TOP_USP_2) or !empty($text.TOP_USP_3)}
                <div class="pull-left is-inline-block m-usp-wrap is-hidden-s">
                    {if !empty($text.TOP_USP_1)}
                        {if $languages->getActualSize() gt 1 || $currencies and $currencies->getActualSize() gt 1}|{/if}<span class="m-usp is-inline-block"><span class="m-usp__text">{$text.TOP_USP_1}</span><i class="fa fa-check fa-lg"></i></span>
                    {/if}
                    {if !empty($text.TOP_USP_2)}
                        |<span class="m-usp is-inline-block"><span class="m-usp__text">{$text.TOP_USP_2}</span><i class="fa fa-check fa-lg"></i></span>
                    {/if}
                    {if !empty($text.TOP_USP_3)}
                        |<span class="m-usp is-inline-block"><span class="m-usp__text">{$text.TOP_USP_3}</span><i class="fa fa-check fa-lg"></i></span>
                    {/if}
                </div>
                {/if}                  
                
                {if $template.settings.SETTINGS_SHOW_SEARCH}
                    <div class="search-module pull-right">
                        <form class="search-form" method="get" action="/{if $general.isShop}{page id=$page.productPageId print=Link}{else}{$Text.SEARCH_LINK}{/if}/">
                            <label aria-label="{$Text.SEARCH_TEXT}" class="corporate__search">
                                <input type="text" class="corporate__searchinput" placeholder="{$Text.SEARCH_TEXT}" name="search" required>
                                <button class="corporate__searchbutton" title="{$text.SEARCH}" type="submit"><i class="fa fa-search fa-lg"></i></button>
                            </label>
                        </form>
                    </div>
                {/if}
            </div>
            <div class="col-s-4 col-m-12 col-l-12 col-xl-24">
                <hr>
            </div>
        </div>
    </div>
</div>
{/if}
<div class="container container--header">
    <header class="site-header">
        <div class="row">
            <div class="col-s-4 col-m-12 col-l-12 col-xl-24">
                {include file='modules/widgets/logo/logo.tpl'}
            </div>
        </div>
    </header>
</div>
<div class="container container--navigation">
    <div class="site-navigation">
        <div class="row">
            {if $general.isShop && $template.settings.SETTINGS_SHOW_CART && $page.type != 'cart' && $page.type != 'checkout'}
            <div class="col-s-4 col-m-8 col-l-8 col-xl-16">
            {else}
            <div class="col-s-4 col-m-12 col-l-12 col-xl-24">
            {/if}
                <nav class="navbar navbar-tablet-full nav-desktop-m w-widget widget">

                    <div class="navbar-header">
                        <button type="button" class="button-primary navbar-toggle pull-left" title="{$text.WHERE_AM_I_NAVIGATION}" data-toggle="collapse" data-target="main-navbar"><i class="fa fa-bars fa-fw"></i></button>

                        {if $languages->getActualSize() gt 1 || $currencies and $currencies->getActualSize() gt 1}
                            {if $currencies and $currencies->getActualSize() gt 1}
                                <div class="w-currency-picker is-inline-block is-visible-s pull-left">
                                    {include file="modules/widgets/currency/currency.tpl" collection=$currencies type="dropdown"}
                                </div>
                            {/if}

                            {if $languages->getActualSize() gt 1}
                                <div class="w-language-picker is-inline-block is-visible-s pull-left">
                                    {$showFlag = true}
                                    {$showText = true}
                                    {if $template.settings.SETTINGS_TYPE_LANGUAGE eq 'FLAG'}
                                        {$showText = false}
                                    {elseif $template.settings.SETTINGS_TYPE_LANGUAGE eq 'TEXT'}
                                        {$showFlag = false}
                                    {/if}

                                    {include file="modules/widgets/language/language.tpl" collection=$languages type="dropdown" showFlag=$showFlag showText=$showText}
                                </div>
                            {/if}
                        {/if}
                        {if $general.isShop && $template.settings.SETTINGS_SHOW_CART && $page.type != 'cart' && $page.type != 'checkout'}
                        <div class="cart-module is-visible-s">
                            {* Cart controller *}
                            {collection controller=cart assign=cart}
                            {include file='modules/widgets/cart/cart.tpl' cart=$cart hasDropdown=false}
                        </div>
                        {/if}
                    </div>

                    <div class="navbar-collapse is-collapsed" data-group="main-navbar">
                        <div class="row">

                            <div class="col-s-4 col-m-12 col-l-12 col-xl-24">

                                {$static = false}
                                {if $template.settings.SETTINGS_SHOW_MY_ACCOUNT}
                                    {$static = true}
                                {/if}

                                {menu assign=primaryMenu static=$static}

                                {include
                                    file='modules/widgets/menu/menu.tpl'
                                    items=$primaryMenu
                                    classes='nav nav-default'
                                }
                            </div>
                        </div>
                    </div>
                </nav>
            </div>


            {if $general.isShop && $template.settings.SETTINGS_SHOW_CART && $page.type != 'cart' && $page.type != 'checkout'}
            <div class="col-s-4 col-m-4 col-l-4 col-xl-8 is-hidden-s">
                <div class="cart-module pull-right">
                    {* Cart controller *}
                    {collection controller=cart assign=cart}
                    {include file='modules/widgets/cart/cart.tpl' cart=$cart}
                </div>
            </div>
            {/if}

        </div>
    </div>
</div>
