# Classic Theme #
============

The Classic Theme is a sub-theme of SmartWeb, inheriting template files from the main theme Rooty. It can be used by those wanting to use a theme with a smaller width and font-size.

**Demo:**

* [Demo store](http://baby-theme.smartweb.dk/)


## Documentation ##

* [Getting started - in Danish](http://design-help-new.smart-web.dk/35-template-udvikling/).
* [Theme Documentation - in Danish](http://design-help-new.smart-web.dk/3-for-udviklere/).
* Need more help? Contact our [Support](http://www.get-smartweb.com/support/).
